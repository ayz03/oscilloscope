//coder: hassin ayaz 
#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>
#include "ks0108.h"

void plot_total_screen(void);
void adc_init(void);

volatile uint8_t v[128];    // global variable for holding volatge values at each pixel 1 px = 1 sample 
volatile uint8_t temp[128];
volatile uint8_t t;        //time 1px=1time loop variable 
volatile uint8_t read_adc_value(uint8_t);             //sub function for adc value reading


void main(void)
{
//uint8_t x ;

uint8_t adc_8;

adc_init();       //initialize adc 

//adc_8=read_adc_value(0);

/*for(t=0; t<=127; t++)
   {
   v[t] = adc_8;                //all values are set to current adc value 
   }*/

_delay_ms(2000);  // Wait a little while the display starts up

ks0108Init(0);    // Initialize the LCD

ks0108ClearScreen();  //clear total screen 

/*for(x=0; x<=127; x++)
   {
   ks0108SetDot(x,23,BLACK);
   }*/

//plot_total_screen();   
   
while(1)
     {
	 for(t=0; t<=127; t++)
	    {
		temp[t]=v[t];
		}
	 adc_8=read_adc_value(0);
	 v[127]=(int)((adc_8*63)/255);
	 for(t=0; t<=126; t++)
        {
		v[t]=v[t+1];
        }
 	 plot_total_screen();
	 //ks0108ClearScreen();  //clear total screen
	 }   
}

void adc_init(void)
{
ADMUX|=(1<<REFS0);               //AVCC with external capacitor at AREF
ADCSRA|=(1<<ADEN);               //ADC is enabled but no conversion is started
ADCSRA|=(1<<ADPS2);              //1MHZ/16=62.5KHZ
ADMUX|=(1<<ADLAR);               //result is left adjusted and only ADCH will be read s 8 bit presision is required 
}

uint8_t read_adc_value(uint8_t state)
{
uint16_t value_adc;
state=state&0b00000111;
ADMUX&=0b11111000;           //clear bottom 3 bits
ADMUX|=state;                //set to new channel 
ADCSRA|=(1<<ADSC);           //ADC was enabled but conversion didnot start until now
while(!(ADCSRA&(1<<ADIF)))   //wait until conversion is completed ie until ADIF flag is set 
     {;}
ADCSRA|=(1<<ADIF);           //ADIF is 0 now

/*value_adc=ADCL;        //ADCL data register read first as result is right adjusted
value_adc+=(ADCH<<8);  //ADCH data register read*/

value_adc = ADCH;      //ADCH data register read first as result is left adjusted

return(value_adc);
}

void plot_total_screen(void)
{
for(t=0; t<=127; t++)
   {
   if(v[t]!=temp[t])
     {
	 ks0108SetDot(t,(63-temp[t]),WHITE);
     ks0108SetDot(t,(63-v[t]),BLACK);
	 }
   else
     {
	 }
   }
}