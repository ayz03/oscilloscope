//atmega8 comparator test 

#include <avr/io.h>
#include <avr/interrupt.h> 

void analog_comp_init(void);

void main(void)
{

DDRC&=~(1<<1);       //ADC1 pin set as input 
PORTC|=(1<<1);        //no internal pull up 

DDRD|=(1<<5);         //LED output 
PORTD&=~(1<<5);        //LED off 

analog_comp_init();

//sei();

while(1)
     {
     if (bit_is_clear(ACSR,ACO))              //responsed but not useful
     PORTD|= (1<<5);   //LED is on
     else
     PORTD&=~(1<<5);  //LED is off 
     }
}


void analog_comp_init(void)
{

SFIOR|=(1<<ACME);     //enable multiplexer 

ADCSRA&=~(1<<ADEN);   // ADC is switched off for multiplexer selection 

ADMUX|=(0<<MUX2)|(0<<MUX1)|(1<<MUX0);  //ADC connected to AIN1 negative input of analog comparator 

//ACSR|=(0<<ACD)|(1<<ACBG)|(1<<ACIE)|(0<<ACIC)|(1<<ACIS1)|(1<<ACIS0); //interrupt on rising edge

ACSR|=(0<<ACD)|(1<<ACBG)|(0<<ACIC)|(1<<ACIS1)|(1<<ACIS0); //interrupt on rising edge

}

/*ISR(ANA_COMP_vect)       //did not responsed
{
if (bit_is_clear(ACSR,ACO))
   
   PORTD|= (1<<5);   //LED is on
else
   
   PORTD&=~(1<<5);  //LED is off 

}*/