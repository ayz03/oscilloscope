
#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>


#include "ks0108.h"
#include "uart.h"

void plot_total_screen(void);
void adc_init(void);
void timer_init(void);

volatile uint8_t v[128];    // global variable for holding volatge values at each pixel 1 px = 1 sample 
//volatile uint8_t temp[128];
//volatile uint8_t t;        //time 1px=1time loop variable 
volatile uint8_t read_adc_value(uint8_t);             //sub function for adc value reading

volatile unsigned int T1capture,lastT1capture,period,delay;

volatile char read_adc = 0 ; 

//UART file descriptor putchar and getchar are in uart.c
//FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);
//float freq;
//char freq_string[10];                               //scaled input frequency string to print

void main(void)
{
uint16_t n;        //number of adc readings 

uint8_t adc_8;

DDRD|=(1<<5);

adc_init();       //initialize adc 
timer_init();     //initialize timer
//uart_init();
//stdout = stdin = &uart_str;
//fprintf(stdout,"Frequency meter ....\n\r");

ks0108Init(0);    // Initialize the LCD

ks0108ClearScreen();  //clear total screen  

sei();             //global interrupt enabled
   
while(1)
     {
	 PORTD&=~(1<<5);
	 //freq=(8000000.0/(float)period);
	 if(period!=T1capture)
	   {
	   if(read_adc==1)
	     {
	     delay=(int)((period*0.125)/128);
		 //delay=(int)((period*1)/128);
	     _delay_us(n*delay);
		 adc_8=read_adc_value(0);
	     v[n]=(int)((adc_8*63)/255);
		 read_adc=0;
	     n++;
	     if(n==128)
	       {
		   cli();
		   plot_total_screen();
		   n=0;
		   sei();
		   }
		 }
	   }
	// dtostrf(freq,6,3,freq_string);
	// printf("freq=%sHZ\n\r",freq_string);
	// printf("period=%d of pulses\n\r",period);
	 }   
}

void adc_init(void)
{
ADMUX|=(1<<REFS0);               //AVCC with external capacitor at AREF
ADCSRA|=(1<<ADEN);               //ADC is enabled but no conversion is started
//ADCSRA|=(1<<ADPS1)|(1<<ADPS0);   //8MHZ/8=1MHZ
ADCSRA|=(1<<ADPS2);              
ADMUX|=(1<<ADLAR);               //result is left adjusted and only ADCH will be read s 8 bit presision is required 

ACSR|=(1<<ACBG)|(1<<ACIC);      //internal bandgap ref of 2.56v is selected to pos AIN0 pin
                                 //Timer/Counter1 to be triggered by the Analog Comparator
}

uint8_t read_adc_value(uint8_t state) //8 bit conversion 
{
uint16_t value_adc;
state=state&0b00000111;
ADMUX&=0b11111000;           //clear bottom 3 bits
ADMUX|=state;                //set to new channel 
ADCSRA|=(1<<ADSC);           //ADC was enabled but conversion didnot start until now
while(!(ADCSRA&(1<<ADIF)))   //wait until conversion is completed ie until ADIF flag is set 
     {;}
ADCSRA|=(1<<ADIF);           //ADIF is 0 now

value_adc = ADCH;      //ADCH data register read first as result is left adjusted

return(value_adc);
}

void timer_init(void)
{
TCCR1B|=(1<<ICES1)|(1<<CS10); //a rising (positive) edge will trigger the capture , no prescalling so 8MHz
//TCCR1B|=(1<<ICES1)|(1<<CS11); //a rising (positive) edge will trigger the capture , 8 prescalling so 1MHz
TIMSK|=(1<<TICIE1);            // Input Capture Interrupt Enable
}


ISR(TIMER1_CAPT_vect)
{
read_adc = 1;
PORTD|=(1<<5);
T1capture=ICR1 ;                          //read timer1 input capture register
period= T1capture-lastT1capture;          //compute time between captures
lastT1capture=T1capture ;
}

void plot_total_screen(void)
{
ks0108ClearScreen();                       //clear total screen 
uint8_t t;
for(t=0; t<=127; t++)
   {
   //ks0108SetDot(t,(63-temp[t]),WHITE);
   ks0108SetDot(t,(63-v[t]),BLACK);
   }
/*for(t=0; t<=126; t++)
   {
   ks0108DrawLine(t, v[t], (t+1), v[t+1], BLACK);
   }*/
}