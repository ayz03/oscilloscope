//coder: hassin ayaz 
//((4000000/(16*9600))-1)=25.04=25 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "ks0108.h"

#define PX 128

void adc_init(void);
void extrn_intrr_init(void);
unsigned char read_adc_value(unsigned char);             //sub function for adc value reading
void USART_init(unsigned int ubrr_value);
void USART_transmit(unsigned char data);

//volatile unsigned char start_reading = 0;
//volatile unsigned char cmd;

volatile unsigned char v[PX];       //variable for holding volatge values at each pixel 1 px = 1 sample  260
volatile unsigned int t;            //time 1px=1time loop variable 
volatile unsigned char adc;          //raw adc


void main(void)
{

DDRB|=(1<<0);
DDRB|=(1<<1);

PORTB|=(1<<0);
PORTB|=(1<<1);

DDRD&=~(1<<2);              //from comparator

//unsigned char adc;          //raw adc 
//unsigned char v[260];       //variable for holding volatge values at each pixel 1 px = 1 sample
//unsigned int t;            //time 1px=1time loop variable 
 
adc_init();       //initialize adc

extrn_intrr_init();
//USART_init(25);
  
ks0108Init(0);    // Initialize the LCD
ks0108ClearScreen();  //clear total screen 
  
sei();           

//_delay_ms(100);  
   
while(1)
     {
/*	 if(start_reading == 1)
	   {
       //PORTB&=~(1<<0);
       //PORTB|=(1<<0);             //pulse */
/*	   for(t=0; t<260; t++)
	      {
		  //adc=read_adc_value(1);        
		  //v[t] = (int)((adc*63)/255);
		  v[t]=read_adc_value(1);
		  //v[t]=adc;
		  }
       //PORTB&=~(1<<1);            //pulse
       //PORTB|=(1<<1);      
       //ks0108ClearScreen();  //clear total screen
       
	   for(t=0; t<260; t++)
          {
		  USART_transmit(v[t]);
		  //_delay_ms(10);
          //ks0108SetDot(t,(63-v[t]),BLACK);
	      }
       //start_reading = 0;        
	   //}*/

	 //GICR|=(1<<INT0);        //interrupt for INT0 pin request is enabled
     //ACSR |= (1<<ACIE);     //a/d comparator interrupt turned on*/
	 
	 /*adc=read_adc_value(1);
	 USART_transmit(adc);
	 _delay_ms(10);*/
	 }   
}

void adc_init(void)
{
ADMUX|=(1<<REFS0);               //AVCC with external capacitor at AREF
ADCSRA|=(1<<ADEN);               //ADC is enabled but no conversion is started
ADCSRA|=(1<<ADPS1);              //4MHZ/4=1MHZ
ADMUX|=(1<<ADLAR);               //result is left adjusted and only ADCH will be read s 8 bit presision is required 
}

void extrn_intrr_init(void)
{
MCUCR|=(1<<ISC01);      //the falling edge of INT0  will trigger an interrupt telling signal just got greater than ref voltage
GICR|=(1<<INT0);        //interrupt for INT0 pin request is enabled
}

ISR(INT0_vect)
{
//GICR&=~(1<<INT0);    //INT0 disabled 
//start_reading = 1;    //data reading starting flag turned on
PORTB&=~(1<<0);
PORTB|=(1<<0);             //pulse */

for(t=0; t<PX; t++)
   {
   //v[t]=read_adc_value(1);
   
   adc=read_adc_value(1);        
   v[t] = (int)((adc*63)/255);
   }
PORTB&=~(1<<1);            //pulse
PORTB|=(1<<1); 

for(t=0; t<PX; t++)
   {
   //USART_transmit(v[t]);
   ks0108SetDot(t,(63-v[t]),BLACK);
   }   
}

/*void ad_comp_init(void)
{
ACSR &=~ (1<<ACD);
ACSR |= (1<<ACIS0)|(1<<ACIS1);   //AC0 bit goes from 0 to 1 interrupt is thrown 
//ACSR |= (1<<ACIS1);
//ACSR |= (1<<ACIE);                //a/d comparator interrupt turned on
ACSR|=(1<<ACBG)|(1<<ACIE);      //internal bandgap ref of 2.56v is selected to pos AIN0 pin
}

ISR(ANA_COMP_vect)
{
if(ACSR&(1<<ACO))
  {
  PORTB |= (1<<5);
  _delay_ms(300); 
  PORTB &=~(1<<5);
  }
//ACSR &=~ (1<<ACIE);   //a/d comparator interrupt turned off until 1 block of data is read / post trigger 
//tart_reading = 1;    //data reading starting flag turned on 
}*/

unsigned char read_adc_value(unsigned char state)
{
unsigned char value_adc;
state=state&0b00000111;
ADMUX&=0b11111000;           //clear bottom 3 bits
ADMUX|=state;                //set to new channel 
ADCSRA|=(1<<ADSC);           //ADC was enabled but conversion didnot start until now
while(!(ADCSRA&(1<<ADIF)))   //wait until conversion is completed ie until ADIF flag is set 
     {;}
ADCSRA|=(1<<ADIF);           //ADIF is 0 now

/*value_adc=ADCL;        //ADCL data register read first as result is right adjusted
value_adc+=(ADCH<<8);  //ADCH data register read*/

value_adc = ADCH;      //ADCH data register read first as result is left adjusted

return(value_adc);
}

void USART_init(unsigned int ubrr_value)
{
UBRRH=(unsigned char)(ubrr_value>>8);
UBRRL=(unsigned char)ubrr_value;

//UCSRB = (1<<RXCIE)|(1<<TXEN)|(1<<RXEN);   //interrupt on recieve 
UCSRB = (1<<TXEN)|(1<<RXEN);   
UCSRC=(1<<URSEL)|(3<<UCSZ0);

}

void USART_transmit(unsigned char data)  
{                              
while(!(UCSRA&(1<<UDRE)))      
     {}                       
UDR=data;                      
}

/*ISR(USART_RXC_vect)
{
//PORTB|=(1<<0);

//while(!(UCSRA & (1<<RXC)))   //Wait for data to be received
//	;

cmd = (unsigned char)UDR;


/*if(cmd=='s')           //slow command for roll mode got
  {
  if(delay>0)
    {
	delay=(delay-10);
	}
  }
else if(cmd=='f')      //fast command for roll mode got
  {
  if(delay<1500)
    {
	delay=(delay+10);
	}
  }
  
  
else if(cmd=='x')      //Roll mode selection command got 
  {
  mode=1;
  ADC_init_roll();
  }
else if(cmd=='y')      //Single shot / trigger mode selection command got
  {
  mode=2;
  ADC_init_trig(1);     
  }
  
  
else if(cmd=='a')                              //50mV/div
  {
  PORTB|=(1<<3);   //R1   50mV/div
  PORTB&=~(1<<4);  //R2   500mV/div
  PORTB&=~(1<<5);  //R3   5V/div
  }
else if(cmd=='b')                              //500mV/div
  {
  PORTB&=~(1<<3);  //R1   50mV/div
  PORTB|=(1<<4);   //R2   500mV/div
  PORTB&=~(1<<5);  //R3   5V/div
  }
else if(cmd=='c')                              //5V/div
  {
  PORTB&=~(1<<3);  //R1   50mV/div
  PORTB&=~(1<<4);  //R2   500mV/div
  PORTB|=(1<<5);   //R3   5V/div
  }
  
  
  
else
  {}
}*/


