//coder: hassin ayaz 
//((8000000/(16*9600))-1)=51.08=51 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "ks0108.h"

#define PX 260

void adc_init(unsigned char state);
void extrn_intrr_init(void);
unsigned char read_adc_value(void);             //sub function for adc value reading

//void USART_init(unsigned int ubrr_value);
//void USART_transmit(unsigned char data);


volatile unsigned char v[PX];       //variable for holding volatge values at each pixel 1 px = 1 sample  260
volatile unsigned int t;            //time 1px=1time loop variable 
//volatile unsigned char adc;          //raw adc

volatile unsigned char trig=0;
volatile unsigned char cmd;


void main(void)
{
DDRD|=(1<<6);              //to time calculation
DDRD|=(1<<7);

PORTD|=(1<<6);              
PORTD|=(1<<7);

DDRD&=~(1<<2);              //from comparator

 
adc_init(1);       //initialize adc

extrn_intrr_init();

USART_init(51);
  
ks0108Init(0);    // Initialize the LCD
ks0108ClearScreen();  //clear total screen 
  
sei();           

 
   
while(1)
     {
	 if(trig==1)
	   {
	   GICR&=~(1<<INT0);        //interrupt for INT0 pin request is disabled
	   
	   PORTD&=~(1<<6);
       PORTD|=(1<<6);             //pulse 
	   for(t=0; t<PX; t++)
          {
          v[t]=read_adc_value();
          }
	   PORTD&=~(1<<7);            //pulse
       PORTD|=(1<<7);
	   
	   //ks0108ClearScreen();  //clear total screen 
	   for(t=0; t<PX; t++)
          {
          //ks0108SetDot(t,(63-(int)((v[t]*63)/255)),BLACK);
		  USART_transmit(v[t]);
          }
	   GICR|=(1<<INT0);        //interrupt for INT0 pin request is enabled 
	   trig=0; 
	   }
	 
	 
	 
	 }   
}

void adc_init(unsigned char state)
{
ADMUX|=(1<<REFS0);               //AVCC with external capacitor at AREF
ADCSRA|=(1<<ADEN);               //ADC is enabled but no conversion is started
//ADCSRA|=(1<<ADPS1);              //8MHZ/4=2MHZ

ADCSRA|=(1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2); //8MHz/128=62.5KHz

ADMUX|=(1<<ADLAR);               //result is left adjusted and only ADCH will be read s 8 bit presision is required

state=state&0b00000111;
ADMUX&=0b11111000;           //clear bottom 3 bits
ADMUX|=state;   
}

void extrn_intrr_init(void)
{
MCUCR|=(1<<ISC01);      //the falling edge of INT0  will trigger an interrupt telling signal just got greater than ref voltage
GICR|=(1<<INT0);        //interrupt for INT0 pin request is enabled
}

ISR(INT0_vect)
{
/*ks0108ClearScreen();  //clear total screen 

PORTD&=~(1<<6);
PORTD|=(1<<6);             //pulse 

for(t=0; t<PX; t++)
   {
   v[t]=read_adc_value();
   
   //adc=read_adc_value();        
   //v[t] = (int)((adc*63)/255);
   }
PORTD&=~(1<<7);            //pulse
PORTD|=(1<<7); 

for(t=0; t<PX; t++)
   {
   //USART_transmit(v[t]);
   ks0108SetDot(t,(63-(int)((v[t]*63)/255)),BLACK);
   }   */
   
trig=1;   
   
}


unsigned char read_adc_value(void)
{
//unsigned char value_adc;
ADCSRA|=(1<<ADSC);           //ADC was enabled but conversion didnot start until now
while(!(ADCSRA&(1<<ADIF)))   //wait until conversion is completed ie until ADIF flag is set 
     {;}
ADCSRA|=(1<<ADIF);           //ADIF is 0 now

/*value_adc=ADCL;        //ADCL data register read first as result is right adjusted
value_adc+=(ADCH<<8);  //ADCH data register read*/

//value_adc = ADCH;      //ADCH data register read first as result is left adjusted

//return(value_adc);
return(ADCH);
}

void USART_init(unsigned int ubrr_value)
{
//UBRRH=(unsigned char)(ubrr_value>>8);
UBRRL=(unsigned char)ubrr_value;

UCSRB = (1<<RXCIE)|(1<<TXEN)|(1<<RXEN);   //interrupt on recieve 
//UCSRB = (1<<TXEN)|(1<<RXEN);   
//UCSRC=(1<<URSEL)|(3<<UCSZ0);

UCSRC=(1<<URSEL);
UCSRC|=(1<<UCSZ0);
UCSRC|=(1<<UCSZ1);
}

void USART_transmit(unsigned char data)  
{                              
while(!(UCSRA&(1<<UDRE)))      
     {}                       
UDR=data;                      
}

ISR(USART_RXC_vect)
{
//PORTB|=(1<<0);

//while(!(UCSRA & (1<<RXC)))   //Wait for data to be received
//	;

cmd = (unsigned char)UDR;

switch(cmd)
	  {
      case 'a': 
	          ADCSRA&=~(1<<ADPS0);  
			  ADCSRA|=(1<<ADPS1);
			  ADCSRA&=~(1<<ADPS2);  //010
			  break;
      case 'b': 
	          ADCSRA|=(1<<ADPS0);   
			  ADCSRA|=(1<<ADPS1);
			  ADCSRA&=~(1<<ADPS2);  //011
			  break;
      case 'c': 
	          ADCSRA&=~(1<<ADPS0);
			  ADCSRA&=~(1<<ADPS1);
			  ADCSRA|=(1<<ADPS2);   //100
			  break;
      case 'd': 
	          ADCSRA|=(1<<ADPS0);
			  ADCSRA&=~(1<<ADPS1);
			  ADCSRA|=(1<<ADPS2);   //101
			  break;
      case 'e': 
	          ADCSRA&=~(1<<ADPS0);
			  ADCSRA|=(1<<ADPS1);
			  ADCSRA|=(1<<ADPS2);   //110
			  break;
      case 'f': 
	          ADCSRA|=(1<<ADPS0);
			  ADCSRA|=(1<<ADPS1);
			  ADCSRA|=(1<<ADPS2);
			  break;	
	  
      }
}


/*if(cmd=='s')           //slow command for roll mode got
  {
  if(delay>0)
    {
	delay=(delay-10);
	}
  }
else if(cmd=='f')      //fast command for roll mode got
  {
  if(delay<1500)
    {
	delay=(delay+10);
	}
  }
  
  
else if(cmd=='x')      //Roll mode selection command got 
  {
  mode=1;
  ADC_init_roll();
  }
else if(cmd=='y')      //Single shot / trigger mode selection command got
  {
  mode=2;
  ADC_init_trig(1);     
  }
  
  
else if(cmd=='a')                              //50mV/div
  {
  PORTB|=(1<<3);   //R1   50mV/div
  PORTB&=~(1<<4);  //R2   500mV/div
  PORTB&=~(1<<5);  //R3   5V/div
  }
else if(cmd=='b')                              //500mV/div
  {
  PORTB&=~(1<<3);  //R1   50mV/div
  PORTB|=(1<<4);   //R2   500mV/div
  PORTB&=~(1<<5);  //R3   5V/div
  }
else if(cmd=='c')                              //5V/div
  {
  PORTB&=~(1<<3);  //R1   50mV/div
  PORTB&=~(1<<4);  //R2   500mV/div
  PORTB|=(1<<5);   //R3   5V/div
  }
  
  
  
else
  {}
}

/*void ad_comp_init(void)
{
ACSR &=~ (1<<ACD);
ACSR |= (1<<ACIS0)|(1<<ACIS1);   //AC0 bit goes from 0 to 1 interrupt is thrown 
//ACSR |= (1<<ACIS1);
//ACSR |= (1<<ACIE);                //a/d comparator interrupt turned on
ACSR|=(1<<ACBG)|(1<<ACIE);      //internal bandgap ref of 2.56v is selected to pos AIN0 pin
}

ISR(ANA_COMP_vect)
{
if(ACSR&(1<<ACO))
  {
  PORTB |= (1<<5);
  _delay_ms(300); 
  PORTB &=~(1<<5);
  }
//ACSR &=~ (1<<ACIE);   //a/d comparator interrupt turned off until 1 block of data is read / post trigger 
//tart_reading = 1;    //data reading starting flag turned on 
}*/



