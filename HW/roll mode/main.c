//coder: hassin ayaz 
//((8000000/(16*9600))-1)=51.08=51 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define SAMPLES_WIDTH 256  //45 512

void adc_init(unsigned char state);
unsigned char read_adc_value(void);             //sub function for adc value reading

void USART_init(unsigned int ubrr_value);
void USART_transmit(unsigned char data);

void extrn_intrr_init(void);

volatile unsigned char cmd;
//volatile unsigned int delay=65;

volatile unsigned int n;
volatile unsigned char frame[SAMPLES_WIDTH];

volatile unsigned char trig=0;
volatile unsigned char mode=2;

void main(void)
{
//unsigned char adc;
DDRB|=(1<<0);              //to time calculation
DDRB|=(1<<1);

PORTB|=(1<<0);              
PORTB|=(1<<1);

DDRB|=(1<<3);              //to range determining ckt
DDRB|=(1<<4);
DDRB|=(1<<5);

PORTB&=~(1<<3);            //range3 selected initially -50V to +50V
PORTB&=~(1<<4);
PORTB|=(1<<5);

adc_init(1);       //initialize adc
USART_init(51);
extrn_intrr_init();
sei();
    
while(1)
     {
	 //adc=read_adc_value();
     if(mode==3)                      //triggered frame
       {
       if(trig==1)
         {
	     GICR&=~(1<<INT0);    //INT0 disabled 
	     //PORTB&=~(1<<0);
         //PORTB|=(1<<0);             //pulse 
	     for(n=0; n<SAMPLES_WIDTH; n++)
	        {
		    frame[n]=read_adc_value();		
		    }
	     //PORTB&=~(1<<1);            //pulse
         //PORTB|=(1<<1);
		//USART_transmit(255);
		//USART_transmit(0);		 
	     for(n=0; n<SAMPLES_WIDTH; n++)
	        {
		    USART_transmit(frame[n]);	
		    }
	     trig=0;
	     GICR|=(1<<INT0);
         } 	   
       }
     else if(mode==2)                  //continuous frame
       {
	   //PORTB&=~(1<<0);
       //PORTB|=(1<<0);             //pulse 	   
	   for(n=0; n<SAMPLES_WIDTH; n++)
	      {		  
		  frame[n]=read_adc_value();		
		  }
	   //PORTB&=~(1<<1);            //pulse
       //PORTB|=(1<<1);		  
	   for(n=0; n<SAMPLES_WIDTH; n++)
	      {
		  USART_transmit(frame[n]);				  
		  }	   
       }
     else if(mode==1)                  //roll mode
       {
	   //PORTB&=~(1<<0);
       ///PORTB|=(1<<0);                //pulse              	   
       USART_transmit(read_adc_value());
       _delay_ms(99);	
	   //PORTB&=~(1<<1);
       //PORTB|=(1<<1);             //pulse 	 
       }
	 else{}
	 }   
}

void adc_init(unsigned char state)
{
ADMUX|=(1<<REFS0);               //AVCC with external capacitor at AREF
ADCSRA|=(1<<ADEN);               //ADC is enabled but no conversion is started

ADCSRA|=(1<<ADPS2);
ADCSRA&=~(1<<ADPS1);
ADCSRA|=(1<<ADPS0); 	       //prescalar 32

ADMUX|=(1<<ADLAR);               //result is left adjusted and only ADCH will be read s 8 bit presision is required
state=state&0b00000111;
ADMUX&=0b11111000;           //clear bottom 3 bits
ADMUX|=state;   
}


unsigned char read_adc_value(void)
{
ADCSRA|=(1<<ADSC);           //ADC was enabled but conversion didnot start until now
while(!(ADCSRA&(1<<ADIF)))   //wait until conversion is completed ie until ADIF flag is set 
     {;}
ADCSRA|=(1<<ADIF);           //ADIF is 0 now
return(ADCH);
}
void extrn_intrr_init(void)
{
DDRD&=~(1<<2);
MCUCR|=(1<<ISC01);      //the falling edge of INT0  will trigger an interrupt telling signal just got greater than ref voltage
if(mode==3)
  {
  GICR|=(1<<INT0);        //interrupt for INT0 pin request is enabled
  }
else
  {
  GICR&=~(1<<INT0);        //interrupt for INT0 pin request is disabled
  }

}

ISR(INT0_vect)
{
trig=1;    //data reading starting flag turned on
/*PORTB&=~(1<<0);
PORTB|=(1<<0);             //pulse 
for(n=0; n<SAMPLES_WIDTH; n++)
   { 
   frame[n]=read_adc_value();		
   }
PORTB&=~(1<<1);            //pulse
PORTB|=(1<<1);
	 
for(n=0; n<SAMPLES_WIDTH; n++)
   {
   USART_transmit(frame[n]);	
   }*/
}

void USART_init(unsigned int ubrr_value)
{
UBRRH=(unsigned char)(ubrr_value>>8);
UBRRL=(unsigned char)ubrr_value;

UCSRB = (1<<RXCIE)|(1<<TXEN)|(1<<RXEN);   //interrupt on recieve 
//UCSRB = (1<<TXEN)|(1<<RXEN);   
UCSRC=(1<<URSEL)|(3<<UCSZ0);

//UCSRC=(1<<URSEL);
//UCSRC|=(1<<UCSZ0);
//UCSRC|=(1<<UCSZ1);
}

void USART_transmit(unsigned char data)  
{                              
while(!(UCSRA&(1<<UDRE)))      
     {}                       
UDR=data;                      
}

ISR(USART_RXC_vect)
{
//PORTB|=(1<<0); 

cmd = (unsigned char)UDR;

switch(cmd)
	  {
      //Range Selection		  
      case 'a':                  
              PORTB|=(1<<3);            
              PORTB&=~(1<<4);
              PORTB&=~(1<<5);             //range1 -0.25V to 0.25V  ... 0.5V
			  break;
      case 'b':
              PORTB&=~(1<<3);            
              PORTB|=(1<<4);
              PORTB&=~(1<<5);             //range2  -2.5V to 2.5V  ... 5V
			  break;
      case 'c':
              PORTB&=~(1<<3);            
              PORTB&=~(1<<4);
              PORTB|=(1<<5);             //range3 -25V to 25V  ... 50V
			  break;	  
	  //Mode Selection		  
      case 'w':mode=1;/*ADCSRA|=(1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2);*/GICR&=~(1<<INT0);break;
	  case 'x':mode=2;GICR&=~(1<<INT0);break;
	  case 'y':mode=3;GICR|=(1<<INT0);break;
      //ADC Prescalar Selection
      case '1':                          //8
              ADCSRA&=~(1<<ADPS2);
              ADCSRA|=(1<<ADPS1);
              ADCSRA|=(1<<ADPS0);   
              break;            
      case '2':                          //16
	          ADCSRA|=(1<<ADPS2);
			  ADCSRA&=~(1<<ADPS1);
              ADCSRA&=~(1<<ADPS0); 
			  break;
      case '3':                          //32
	          ADCSRA|=(1<<ADPS2);
			  ADCSRA&=~(1<<ADPS1);
              ADCSRA|=(1<<ADPS0); 	 
              break;         
      case '4':                          //64
	  	      ADCSRA|=(1<<ADPS2);
			  ADCSRA|=(1<<ADPS1);
              ADCSRA&=~(1<<ADPS0); 
			  break;
      case '5':	                         //128   >>  8MHz/128=62.5KHz
	          ADCSRA|=(1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2); 
			  break; 
	  default: break;
	  }
}



