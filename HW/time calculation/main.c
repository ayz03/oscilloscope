#include <inttypes.h>
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "uart.h"

void timer1_init(void);
void init_int0_int1(void);

// UART file descriptor
// putchar and getchar are in uart.c
FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

volatile unsigned int ms=0;
volatile unsigned int t1=0;
volatile unsigned int dt=0;

void main(void)
{
init_int0_int1();
timer1_init();
uart_init();
sei();
stdout = stdin = &uart_str;

while(1)
     {
     fprintf(stdout,"dt=%d\n",dt);
	 }
}

void timer1_init(void)
{
TCCR1B|=(1<<WGM12)|(1<<CS11)|(1<<CS10);  //CTC mode enabled;prescalar 1024 selected                                         //6MHZ/1024=5859
TCNT1=0;                                 //timer1 counter register initialized with 0
OCR1A=250;                              //output compare match registerA loaded with 5859
TIMSK|=(1<<OCIE1A);                      //if TCNT1 matches with OCR1A ie when 1 sec is passed a interrupt ocurrs TCNT1 becomes cleared
}

ISR(TIMER1_COMPA_vect)
{
ms++;
}

void init_int0_int1(void)
{
MCUCR|=(1<<ISC01)|(1<<ISC11);            
GICR|=(1<<INT0)|(1<<INT1);            //interrupt for INT0 pin request is enabled
}

ISR(INT0_vect)
{
t1=ms;
}
ISR(INT1_vect)
{
dt=(ms-t1);
}