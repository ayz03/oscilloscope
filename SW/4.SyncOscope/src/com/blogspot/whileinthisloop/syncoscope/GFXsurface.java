package com.blogspot.whileinthisloop.syncoscope;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.util.DisplayMetrics;
import android.util.Log;

public class GFXsurface extends Activity // implements OnTouchListener
{
	final String TAG = "GFXsurface";

	// Message types sent from the BluetoothRfcommClient Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// Key names received from the BluetoothRfcommClient Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String TOAST = "toast";

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;
	// Name of the connected device
	private String mConnectedDeviceName = null;
	// Local Bluetooth adapter
	private BluetoothAdapter mBluetoothAdapter = null;
	// Member object for the RFCOMM services
	private BluetoothRfcommClient mRfcommClient = null;
	
	MyViewSurfaceClass mySurfaceView = null;

	public static boolean ready = true;
	public static int mode = 2;
	public static int range = 3;
	public static int rf = 1;
	public static int d = 1; // time division purpose
	private int k = 0;
	Intent serverIntent = null;

	public static int WIDTH_X = 1024;// 320;1024
	public static int WIDTH_Y = 460;// 240; 448;320
	public static int SAMPLES_WIDTH = 45;

	//public static int WIDTH_X_1_3rd, WIDTH_X_2_3rd;
	//public static int WIDTH_Y_1_3rd, WIDTH_Y_2_3rd;

	public int[] v = new int[SAMPLES_WIDTH];
	public int[] v_r = new int[SAMPLES_WIDTH];
	public int[] v_s = new int[SAMPLES_WIDTH];
	public static int div = 23; // 20: x10 mini pro T7 pro 8 32 

	int x, y;

	private GestureDetector gd;

	DisplayMetrics metrics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/*
		 * DisplayMetrics metrics = new DisplayMetrics();
		 * getWindowManager().getDefaultDisplay().getMetrics(metrics);
		 * WIDTH_X=metrics.widthPixels; WIDTH_Y=metrics.heightPixels;
		 */
		// WIDTH_X = getResources().getDisplayMetrics().widthPixels;
		// WIDTH_Y = getResources().getDisplayMetrics().heightPixels;
		// Set up the window layout

		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.syncoscope_ui);
		mySurfaceView = new MyViewSurfaceClass(this);// this tells the
														// constructor hey i am
														// from this class
		// mySurfaceView.setOnTouchListener(this);
        setContentView(mySurfaceView);
		
		// metrics = new DisplayMetrics();
		// WIDTH_X = getResources().getDisplayMetrics().widthPixels;
		// WIDTH_Y = getResources().getDisplayMetrics().heightPixels;
		// WIDTH_X=metrics.widthPixels;
		// WIDTH_Y=metrics.heightPixels;

		/*
		 * WIDTH_X = mySurfaceView.getWidth(); WIDTH_Y =
		 * mySurfaceView.getHeight();
		 * 
		 * WIDTH_X_1_3rd = ((WIDTH_X)/3); WIDTH_X_2_3rd = ((WIDTH_X*2)/3);
		 * WIDTH_Y_1_3rd = ((WIDTH_Y)/3); WIDTH_Y_2_3rd = ((WIDTH_Y*2)/3);
		 */

		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// If the adapter is null, then Bluetooth is not supported
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available",
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}		
		serverIntent = new Intent(this, DeviceListActivity.class);
		startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		
	}

	@Override
	public void onStart() {
		super.onStart();
		// If BT is not on, request that it be enabled.
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			Toast.makeText(this, "Bluetooth is already enabled",
					Toast.LENGTH_SHORT).show();
			if (mRfcommClient == null)
				setupSync();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mySurfaceView.pause();
	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		mySurfaceView.resume();
		// Toast.makeText(this,
		// String.valueOf(mPlotView.getWidth()),Toast.LENGTH_LONG).show();
		// Toast.makeText(this,
		// String.valueOf(mPlotView.getHeight()),Toast.LENGTH_LONG).show();

		// Performing this check in onResume() covers the case in which BT was
		// not enabled during onStart(), so we were paused to enable it...
		// onResume() will be called when ACTION_REQUEST_ENABLE activity
		// returns.
		if (mRfcommClient != null) {
			// Only if the state is STATE_NONE, do we know that we haven't
			// started already
			if (mRfcommClient.getState() == BluetoothRfcommClient.STATE_NONE) {
				// Start the Bluetooth RFCOMM services
				mRfcommClient.start();
			}
		}

	}

	private void setupSync() {
		// Initialize the BluetoothRfcommClient to perform bluetooth connections
		mRfcommClient = new BluetoothRfcommClient(this, mHandler);
		// mPlotView = (PlotView)findViewById(R.id.PlotArea);
		//mySurfaceView =(MyViewSurfaceClass)findViewById(R.id.GFXsurfaceArea);
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothRfcommClient.STATE_CONNECTED:
					// mBTStatus.setText(R.string.title_connected_to);
					// mBTStatus.append("\n" + mConnectedDeviceName);
					break;
				case BluetoothRfcommClient.STATE_CONNECTING:
					// mBTStatus.setText(R.string.title_connecting);
					break;
				case BluetoothRfcommClient.STATE_NONE:
					// mBTStatus.setText(R.string.title_not_connected);
					break;
				}
				break;
			case MESSAGE_READ: // todo: implement receive data buffering
				byte[] readBuf = (byte[]) msg.obj;
				int data_length = msg.arg1;

				int[] intArray = new int[data_length];

				for (int i = 0; i < data_length; i++) {
					intArray[i] = UByte(readBuf[i]); // worked2
				}
				for (int m = 0; m < intArray.length; m++) {
					if (mode == 1) // roll mode
					{
						if (ready == true) {
							v[(WIDTH_X - 1)] = (int) ((intArray[m] * WIDTH_Y) / 255);
							for (int t = 0; t < (WIDTH_X - 1); t++) {
								v[t] = v[t + 1];
							}
						}
					}

					else if (mode == 2) // one shot mode
					{
						if(ready == true){
							v[k] = (int) ((intArray[m] * WIDTH_Y) / 255);
							k++;
							if (k == SAMPLES_WIDTH) {
								for (int n = 0; n < SAMPLES_WIDTH; n++) {
									v_s[n] = v[n];
								}
								k = 0;
								mySurfaceView.you_can_draw = true;
							}							
						}
					}
					else {
					}
				}
				break;
			case MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				Toast.makeText(getApplicationContext(),
						"Connected to " + mConnectedDeviceName,
						Toast.LENGTH_SHORT).show();
				break;
			case MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(),
						msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
						.show();
				break;
			}
		}

		// signed to unsigned
		private int UByte(byte b) {
			if (b < 0) // if negative
				return (int) ((b & 0x7F) + 128);
			else
				return (int) b;
		}
	};

	/**
	 * Sends a message.
	 * 
	 * @param message
	 *            A string of text to send.
	 */
	private void sendMessage(String message) {
		// Check that we're actually connected before trying anything
		if (mRfcommClient.getState() != BluetoothRfcommClient.STATE_CONNECTED) {
			Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
					.show();
			return;
		}
		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothRfcommClient to write
			byte[] send = message.getBytes();
			mRfcommClient.write(send);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			// When DeviceListActivity returns with a device to connect
			if (resultCode == Activity.RESULT_OK) {
				// Get the device MAC address
				String address = data.getExtras().getString(
						DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				// Get the BLuetoothDevice object
				BluetoothDevice device = mBluetoothAdapter
						.getRemoteDevice(address);
				// Attempt to connect to the device
				mRfcommClient.connect(device);
				// setContentView(mPlotView);
			}
			break;
		case REQUEST_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {
				// Bluetooth is now enabled, so set up the oscilloscope
				setupSync();
			} else {
				// User did not enable Bluetooth or an error occured
				Toast.makeText(this, R.string.bt_not_enabled_leaving,
						Toast.LENGTH_SHORT).show();
				finish();
			}
			break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Bluetooth RFCOMM services
		if (mRfcommClient != null)

			mRfcommClient.stop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Toast.makeText(this,
		// String.valueOf(in_data_size),Toast.LENGTH_LONG).show();
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*Toast.makeText(this, String.valueOf(mySurfaceView.getWidth()),
				Toast.LENGTH_LONG).show();
		Toast.makeText(this, String.valueOf(mySurfaceView.getHeight()),
				Toast.LENGTH_LONG).show();*/
		// Intent serverIntent = null;
		switch (item.getItemId()) {
		case R.id.secure_connect_scan:
			// Launch the DeviceListActivity to see devices and do scan
			serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;

		case R.id.roll_mode:
			//item.setChecked(true);
			mode = 1;
			sendMessage("x"); // roll mode
			// mtd.setText("delay/px");
			// mtd_value.setText(delay_per_px+"ms");
			// mf.setText("No Data");
			return true;

		case R.id.single_shot_mode:
			//item.setChecked(true);
			mode = 2;
			sendMessage("y"); // single shot mode
			// mtd.setText("time/div");
			// mtd_value.setText(time_per_div+"ms");
			return true;
			
		/*case R.id.range:
			if(ready==true){
				range++;
				if(range == 4){range=1;}
				switch (range) {
				case 3:
					sendMessage("3");
					break;
				case 2:
					sendMessage("2");
					break;
				case 1:
					sendMessage("1");
					break;				
				default:
					break;
				}				
			}*/
		case R.id.left:
			rf--;
			if(rf==0){rf=1;}
			return true;
		case R.id.right:
			rf++;
			return true;
		case R.id.up:
			if(ready==true){
				range++;
				if(range==4){range=3;}
				switch (range) {
				case 3:
					sendMessage("3");
					break;
				case 2:
					sendMessage("2");
					break;
				case 1:
					sendMessage("1");
					break;				
				default:
					break;
				}	
			}			
			return true;
		case R.id.down:
			if(ready==true){
				range--;
				if(range==0){range=1;}
				switch (range) {
				case 3:
					sendMessage("3");
					break;
				case 2:
					sendMessage("2");
					break;
				case 1:
					sendMessage("1");
					break;				
				default:
					break;
				}					
			}
		
			return true;			
		default:
			return false;
		}

	}

	// SurfaceView is lighter than View & need a thread thats why Runnable is
	// implemented
	public class MyViewSurfaceClass extends SurfaceView implements Runnable {

		SurfaceHolder myHolder;
		Thread myThread = null; // thread is needed which will update the
								// surface
		boolean isRunning = false;

		boolean you_can_draw = false;
		private Paint mpaint;
		private Paint mwave;
		private Paint mtext;
		private float mFontSize;

		/*
		 * @Override protected void onMeasure(int widthMeasureSpec, int
		 * heightMeasureSpec) { // TODO Auto-generated method stub
		 * super.onMeasure(widthMeasureSpec, heightMeasureSpec); }
		 */

		public MyViewSurfaceClass(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
			myHolder = getHolder(); // holder will hold this surface & control
									// it

			mpaint = new Paint();
			mwave = new Paint();
			mtext = new Paint();

			mwave.setColor(Color.GREEN);
			mwave.setAntiAlias(true);
			mwave.setStyle(Style.FILL_AND_STROKE);
			mwave.setStrokeWidth(2);

			mtext.setColor(Color.YELLOW);
			mwave.setStyle(Style.FILL_AND_STROKE);
			//mFontSize = 25 * getResources().getDisplayMetrics().density;
			mFontSize = 25;
			mtext.setTextSize(mFontSize);
			gd = new GestureDetector(context, new MyGestureClass());
		}

		public void pause() { // this method is called when the calling activity
								// gets paused
			// when activity is paused the thread gonna stop and drawing on
			// canvas will be stopped
			isRunning = false;
			// but at first we must wait until the current thread is stopped the
			// we should break & make our thread null so that it will be stopped
			while (true) {
				try {
					myThread.join(); // Blocks the current Thread until the
										// receiver finishes its execution and
										// dies
				} catch (InterruptedException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				break; // current thread execution is finished so now break
			}
			myThread = null;
		}

		public void resume() {// this method is called when the calling activity
								// is
								// resumed
			isRunning = true;
			myThread = new Thread(this); // "this" tells that thread will use
											// this
			// class's run() to run itself
			myThread.start(); // Thread is now started
		}

		// thread will run by the following method
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (isRunning) {
				if (!myHolder.getSurface().isValid())
					continue; // if invalid then loop again
								// if valid then do the followings
				Canvas canvas = myHolder.lockCanvas();// as surface is valid now
														// Start editing the
														// pixels
														// in the surface
				if (ready == true) {
					canvas.drawColor(Color.rgb(20, 20, 20)); // clear entire
																// screen black
				} else {
					canvas.drawColor(Color.DKGRAY);
				}

				for (int horizontal = 0; horizontal < (WIDTH_Y / div)+1; horizontal++) // 200px/10=20
				{
					if (horizontal == ((WIDTH_Y / div) / 2)) {
						mpaint.setColor(Color.RED);
						canvas.drawLine(0, (horizontal * div), (WIDTH_X - 1),
								(horizontal * div), mpaint);
					} // mmid
					else {
						mpaint.setColor(Color.WHITE);
						canvas.drawLine(0, (horizontal * div), (WIDTH_X - 1),
								(horizontal * div), mpaint);
					} // mgrid
				}
				// draw vertical grids
				for (int vertical = 0; vertical < (WIDTH_X / div)+1; vertical++) // 260px/13=20
				{
					mpaint.setColor(Color.WHITE);
					canvas.drawLine((vertical * div), 0, (vertical * div),
							WIDTH_Y, mpaint); // mgrid
				}
				if (mode == 2) // one shot
				{
					if (you_can_draw == true) {
						for (int n = 0, x = 0; n < (SAMPLES_WIDTH-1) ; x = (x + (rf*div)), n = (n + 1)) {
							canvas.drawLine(x, (WIDTH_Y - v_s[n]), (x + (rf*div)),
									(WIDTH_Y - v_s[n + 1]), mwave); // worked
						}
					}
				} else // roll mode
				{
					for (int n = 0; n < WIDTH_X; n++) {
						v_r[n] = v[n];
					}

					for (int n = 0, x = 0; x < (WIDTH_X - d); x = (x + d), n = (n + 1)) {
						// mpaint.setColor(Color.GREEN);
						canvas.drawLine(x, ((WIDTH_Y - v_r[n])), (x + d),
								((WIDTH_Y - v_r[n + 1])), mwave); // worked
					}
				}
				switch (range) {
				case 3:
					//sendMessage("1");
					canvas.drawText("2.5V/div",0,471,mtext);
					break;
				case 2:
					//sendMessage("2");
					canvas.drawText("250mV/div",0,471,mtext);
					break;
				case 1:
					//sendMessage("3");
					canvas.drawText("25mV/div",0,471,mtext);
					break;

				default:
					break;
				}
				canvas.drawText(+(rf*0.2222)+"ms/div",512,471,mtext);
				myHolder.unlockCanvasAndPost(canvas); // editing is done so now
														// unlock it to show
			}
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			// TODO Auto-generated method stub
			gd.onTouchEvent(event);
			return true;
		}
	}

	public class MyGestureClass extends GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			// TODO Auto-generated method stub
			return super.onDoubleTap(e);
		}

		@Override
		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
			return super.onDown(e);
		}

		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub
			//ready = !ready;
			super.onLongPress(e);
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			// TODO Auto-generated method stub
/*			x = (int) e.getX();
            if(x>512){rf++;}
            else{
            	rf--;
            	if(rf == 0){rf=1;}
            }*/
			ready = !ready;
			return super.onSingleTapConfirmed(e);
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			return super.onSingleTapUp(e);
		}

	}
}
