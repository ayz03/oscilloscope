package com.example.btsync;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

class PlotThread extends Thread
{
	private SurfaceHolder mholder;
	private PlotView mPlotView;
	private boolean _run = false;
	
	public PlotThread(SurfaceHolder surfaceHolder, PlotView view)
	{
		mholder = surfaceHolder;
		mPlotView = view;
	}
	public void setRunning(boolean run)
	{
		_run = run;
	}
	
	@Override
	public void run()
	{
		Canvas c;
		while(_run)
		{
			c = null;
			try
			{
				c = mholder.lockCanvas(null);
				synchronized (mholder) 
				{
                    //Insert methods to modify positions of items in onDraw()
					mPlotView.PlotPoints(c);
				}
			}
			finally
			{
				if(c!=null)
				{
					mholder.unlockCanvasAndPost(c);
				}
			}
		}
	}
}

