package com.example.btsync;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.util.DisplayMetrics;
import android.util.Log;

public class BTSyncActivity extends Activity implements Button.OnClickListener
{
	final String TAG = "BTSyncActivity";
	
	// Message types sent from the BluetoothRfcommClient Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
	// Key names received from the BluetoothRfcommClient Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    
    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the RFCOMM services
    private BluetoothRfcommClient mRfcommClient = null;
     
    //layout
    private Button mbtn_connect = null ;
    private RadioButton rb_a= null;
    private RadioButton rb_b= null;
    private RadioButton rb_c= null;
    
    private RadioButton rb_1= null;
    private RadioButton rb_2= null;
    private RadioButton rb_3= null;
    private RadioButton rb_4= null;
    private RadioButton rb_5= null;
    
    private RadioButton rb_w= null;
    private RadioButton rb_x= null;
    private RadioButton rb_y= null;
    
  //  private ToggleButton mtb = null;
    
    //Run/Pause status

    PlotView mPlotView= null; //***
	public static boolean ready = true; 
	public static int mode = 2;
	public static int range = 3;
	public static int rf = 1;
	public static int d = 1; 
	//private int k = 0;
	private int n ;
	Intent serverIntent = null;
	
	public static int ps = 32;
	
	long t1=0;
	long t2=0;
	static long dt=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
        // Set up the window layout
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		
        serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		
		setContentView(R.layout.real_time_plot);
		// Get local Bluetooth adapter
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) 
        {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
	}

    @Override
    public void onStart() 
    {
        super.onStart();
        // If BT is not on, request that it be enabled.
        if (!mBluetoothAdapter.isEnabled()) 
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } 
        else 
        {
        	Toast.makeText(this, "Bluetooth is already enabled", Toast.LENGTH_SHORT).show();
        	if (mRfcommClient == null) 
        		setupSync();
        }
    }
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}   
    @Override
    public synchronized void onResume()
    {
    	super.onResume();
    	
        //Toast.makeText(this, String.valueOf(mPlotView.getWidth()),Toast.LENGTH_LONG).show();
    	//Toast.makeText(this, String.valueOf(mPlotView.getHeight()),Toast.LENGTH_LONG).show();
    
    	// Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mRfcommClient != null) 
        {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mRfcommClient.getState() == BluetoothRfcommClient.STATE_NONE) 
            {
              // Start the Bluetooth  RFCOMM services
              mRfcommClient.start();
            }
        }
        
    }

    private void setupSync()
    {
    	// Initialize the BluetoothRfcommClient to perform bluetooth connections
        mRfcommClient = new BluetoothRfcommClient(this, mHandler);
        mPlotView = (PlotView)findViewById(R.id.PlotArea);
        //layout buttons pointing 
        
        mbtn_connect = (Button) findViewById(R.id.btn_connect);
        mbtn_connect.setOnClickListener(new OnClickListener(){
			public void onClick(View arg0) {
				BTConnect();		
			}    		
    	}); 
      //---RadioButton--- Range selection 
        rb_a = (RadioButton) findViewById(R.id.rdb_a);
        rb_b = (RadioButton) findViewById(R.id.rdb_b);
        rb_c = (RadioButton) findViewById(R.id.rdb_c);
        
    	RadioGroup radioGroup1 = (RadioGroup) findViewById(R.id.rdbGp1);
    	radioGroup1.setOnCheckedChangeListener(new OnCheckedChangeListener()
    	{
    	public void onCheckedChanged(RadioGroup group, int checkedId) 
    	       {
    	       if(rb_a.isChecked())   //50mV/div
    	         {
    	    	 range=1;
    	    	 sendMessage("a");
    	         } 
    	       else if(rb_b.isChecked())  //500mV/div
    	         {
    	    	 range=2;
    	    	 sendMessage("b"); 
    	         }
    	       else if(rb_c.isChecked())  //5V/div
    	         {
    	    	 range=3;
    	    	 sendMessage("c");  
    	         }
    	       else
    	       {}
    	       }
    	});        
        //---RadioButton--- ADC prescalar Selection 
        rb_1 = (RadioButton) findViewById(R.id.rdb_1);
        rb_2 = (RadioButton) findViewById(R.id.rdb_2);
        rb_3 = (RadioButton) findViewById(R.id.rdb_3);
        rb_4 = (RadioButton) findViewById(R.id.rdb_4);
        rb_5 = (RadioButton) findViewById(R.id.rdb_5);
        
    	RadioGroup radioGroup2 = (RadioGroup) findViewById(R.id.rdbGp2);
    	radioGroup2.setOnCheckedChangeListener(new OnCheckedChangeListener()
    	{
    	public void onCheckedChanged(RadioGroup group, int checkedId) 
    	       {
    	       if(rb_1.isChecked())   //prescalar 8 
    	         {
    	    	 ps=8;
    	    	 sendMessage("1");
    	         } 
    	       else if(rb_2.isChecked())  //16
    	         {
    	    	 ps=16;
    	    	 sendMessage("2"); 
    	         }
    	       else if(rb_3.isChecked())  //32
    	         {
    	    	 ps=32;
    	    	 sendMessage("3");  
    	         }
    	       else if(rb_4.isChecked())  //64
  	            {
    	    	ps=64;
  	    	    sendMessage("4"); 
  	            }
  	           else if(rb_5.isChecked())  //128
  	            {
  	        	ps=128;
  	    	    sendMessage("5");  
  	            }
    	       else
    	       {}
    	       }
    	}); 
        //---RadioButton--- Range selection 
        rb_w = (RadioButton) findViewById(R.id.rdb_w);
        rb_x = (RadioButton) findViewById(R.id.rdb_x);
        rb_y = (RadioButton) findViewById(R.id.rdb_y);
        
    	RadioGroup radioGroup3 = (RadioGroup) findViewById(R.id.rdbGp3);
    	radioGroup3.setOnCheckedChangeListener(new OnCheckedChangeListener()
    	{
    	public void onCheckedChanged(RadioGroup group, int checkedId) 
    	       {
    	       if(rb_w.isChecked())   //roll mode >> mode=1 
    	         {
    	    	 sendMessage("5");
    	    	 rb_5.setChecked(true);
    	    	 ps=128;
    	    	 mode=1;
    	    	 sendMessage("w");   	    	
    	         } 
    	       else if(rb_x.isChecked())  //Continuous Frame >> mode=2
    	         {
    	    	 sendMessage("x");
    	    	 mode=2;
    	         }
    	       else if(rb_y.isChecked())  //Triggered Frame >> mode=3
    	         {
    	    	 sendMessage("y");
    	    	 mode=3;
    	         }
    	       else
    	       {}
    	       }
    	}); 	
    	
    }
        
    private void BTConnect(){
    	serverIntent = new Intent(this, DeviceListActivity.class);
    	startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    	
    } 
        //PlotView.WIDTH_X = mPlotView.getWidth();
        //PlotView.WIDTH_Y = mPlotView.getHeight();  	

     //   mbutton1 = (Button) findViewById(R.id.button1);  // <<fast td--
     //   mbutton2 = (Button) findViewById(R.id.button2);  // >>slow td++      
      
     /*   mbutton2.setOnClickListener(new OnClickListener(){  //   >> slow
			public void onClick(View arg0) {
				if(PlotView.d<20)
				  {
				  PlotView.d++;
				  td=(20/PlotView.d);
				  mtd_value.setText(td+"ms");
				  }

			}    		
    	}); */
    /*	mbutton1.setOnClickListener(new OnClickListener(){  //   << fast
			public void onClick(View arg0) {
				if(PlotView.d>1)
				  {
				  td=(20/PlotView.d);
				  mtd_value.setText(td+"ms");
				  }
			}    		
    	}); */
        
    //	mtd_value = (TextView)findViewById(R.id.td_value);

	//	  mtd_value.setText(td+"ms");     	
        
    /*	RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rdbGp1);
    	radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener()
    	{
    	public void onCheckedChanged(RadioGroup group, int checkedId) 
    	       {
    	       if(rb1.isChecked())   //50mV/div
    	         {
    	    	 sendMessage("1");
    	         } 
    	       else if(rb2.isChecked())  //500mV/div
    	         {
    	    	 sendMessage("2"); 
    	         }
    	       else if(rb3.isChecked())  //5V/div
    	         {
    	    	 sendMessage("3");  
    	         }
    	       else
    	       {}
    	       }
    	});*/
    	
    /*    mtb = (ToggleButton) findViewById(R.id.tbtn_runtoggle);
        mtb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((ToggleButton)v).isChecked())
				 ready=true;
			    else
				 ready=false;
				
			}
		});  */	
   
    

 // The Handler that gets information back from the BluetoothRfcommClient
    private final Handler mHandler = new Handler()
    {
    	@Override
        public void handleMessage(Message msg)
    	{
    		switch (msg.what)
    		{
    		case MESSAGE_STATE_CHANGE:
    			switch (msg.arg1)
    			{
    			case BluetoothRfcommClient.STATE_CONNECTED:
    				//mBTStatus.setText(R.string.title_connected_to);
                    //mBTStatus.append("\n" + mConnectedDeviceName);
    				break;
    			case BluetoothRfcommClient.STATE_CONNECTING:
    				//mBTStatus.setText(R.string.title_connecting);
    				break;
    			case BluetoothRfcommClient.STATE_NONE:
    				//mBTStatus.setText(R.string.title_not_connected);
    				break;
    			}
    			break;
    /////////////////////////////////////////////////////////////////////////////			

/////////////////////////////////////////////////////////////////////////////////				
    		case MESSAGE_READ: // todo: implement receive data buffering
    			if(ready==true){
        			byte[] readBuf = (byte[]) msg.obj;
        			int data_length = msg.arg1;
                    if(mode==1){
                    	for(int i=0; i<data_length; i++){
                    		mPlotView.v[PlotView.SAMPLES_WIDTH-1]=(int)((UByte(readBuf[i])*(PlotView.WIDTH_Y-1))/255);
                    		for(int t=0; t<(PlotView.SAMPLES_WIDTH-1); t++){
                    			mPlotView.v[t]=mPlotView.v[t+1];
                    		}
                    	}
                    }                
                    else{
                    	for(int i=0; i<data_length; i++){
                    		mPlotView.v[n]=(int)((UByte(readBuf[i])*(PlotView.WIDTH_Y-1))/255);
                    		n++;
                			if(n==256){
                				PlotView.you_can_draw=true;
                				n=0;
                			}
                    	}                	
                    }   				
    			}
				break;	
    		case MESSAGE_DEVICE_NAME:
    			// save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                        + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
    			break;
    		case MESSAGE_TOAST:
    			Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                        Toast.LENGTH_SHORT).show();
    			break;
    		}
    	}
    	// signed to unsigned
    	private int UByte(byte b)
    	{
        	if(b<0) // if negative
        		return (int)( (b&0x7F) + 128 );
        	else
        		return (int)b;
        }
    };  
    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message)
    {
    	// Check that we're actually connected before trying anything
    	if (mRfcommClient.getState() != BluetoothRfcommClient.STATE_CONNECTED) 
    	{
    		Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
    		return;
    	}
    	// Check that there's actually something to send
    	if (message.length() > 0) 
    	{
    		// Get the message bytes and tell the BluetoothRfcommClient to write
    		byte[] send = message.getBytes();
    		mRfcommClient.write(send);
    	}
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	switch (requestCode) 
    	{
    	case REQUEST_CONNECT_DEVICE:
    		// When DeviceListActivity returns with a device to connect
    		if (resultCode == Activity.RESULT_OK)
    		{
    			// Get the device MAC address
    			String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
    			// Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mRfcommClient.connect(device);
                //setContentView(mPlotView);
    		}
    		break;
    	case REQUEST_ENABLE_BT:
    		// When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK)
            {
            	// Bluetooth is now enabled, so set up the oscilloscope
            	setupSync();
            }
            else
            {
            	// User did not enable Bluetooth or an error occured
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
    		break;
    	}
    }
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	// Stop the Bluetooth RFCOMM services
        if (mRfcommClient != null)
        
        	mRfcommClient.stop();
    }    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//Toast.makeText(this, String.valueOf(in_data_size),Toast.LENGTH_LONG).show();
    	//Toast.makeText(this, String.valueOf(mPlotView.getWidth()),Toast.LENGTH_LONG).show();
    	//Toast.makeText(this, String.valueOf(mPlotView.getHeight()),Toast.LENGTH_LONG).show();  	
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.btsync, menu);
        return true;
    }    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Intent serverIntent = null;
        switch (item.getItemId()) {
        case R.id.secure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            return true;
     
            
            /* case R.id.insecure_connect_scan:
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
            return true;
        case R.id.discoverable:
            // Ensure this device is discoverable by others
            ensureDiscoverable();
            return true;*/
        
     
// this portion will go to UI        	
/*        case R.id.roll_mode:
        	 mode=1;
	    	 sendMessage("x");    //roll mode
	    	// mtd.setText("delay/px");
	    	// mtd_value.setText(delay_per_px+"ms");
	    	// mf.setText("No Data");
            return true;
            
        case R.id.single_shot_mode:
        	 mode=2;
	    	 sendMessage("y");    //single shot mode 
	    	// mtd.setText("time/div");
	    	// mtd_value.setText(time_per_div+"ms");
            return true;*/
           
        default:
        	return false;
        }
        
    }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}    
}
