package com.example.btsync;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

public class PlotView extends SurfaceView implements SurfaceHolder.Callback 
{
	
	private SurfaceHolder mholder;
	private PlotThread mThread;
	private Paint mpaint;
	private Paint mwave;
	boolean isRunning = false;
	boolean you_can_draw = false;
	private Paint mtext;
	private float mFontSize;	
	
	public static int WIDTH_X = 1024;// 320;1024
	public static int WIDTH_Y = 480;// 240; 448;320   **480
	public static int SAMPLES_WIDTH = 256; //45 512
	public int[] v = new int[SAMPLES_WIDTH];
	public int[] v_r = new int[SAMPLES_WIDTH];
	public int[] v_s = new int[SAMPLES_WIDTH];
	public static int div = 24; // 20: x10 mini pro T7 pro 8 32 23
	int x, y;
	GestureDetector gd;
	DisplayMetrics metrics;
	
	public PlotView(Context context , AttributeSet attrs) 
	{		
		super(context, attrs);
		getHolder().addCallback(this);

		mpaint = new Paint();
		mwave = new Paint();
		mtext = new Paint();
		
		mwave.setColor(Color.GREEN);
		mwave.setAntiAlias(true);
		mwave.setStyle(Style.FILL_AND_STROKE);
		mwave.setStrokeWidth(2);
		
		mtext.setColor(Color.YELLOW);
		mFontSize = 25;
		mtext.setTextSize(mFontSize);	
		//mFontSize = 25 * getResources().getDisplayMetrics().density;
		gd = new GestureDetector(context, new MyGestureClass());

	    mThread = new PlotThread(getHolder(), this);	    
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) 
	{
		// TODO Auto-generated method stub		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		// TODO Auto-generated method stub
        //Start the thread that
	    mThread.setRunning(true);                     //will make calls to 
	    mThread.start();                              //onDraw()
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) 
	{
		// TODO Auto-generated method stub
		boolean retry = true;
		mThread.setRunning(false);
		while (retry)
		{
			try
			{
				mThread.join();
				retry = false;
			}
			catch(InterruptedException e)
			{
			}
		}	
			
	}
	@Override 
    public void onDraw(Canvas canvas) 
	{
        //do drawing stuff here.
		PlotPoints(canvas);
    }
	public void PlotPoints(Canvas canvas)
	{
		if (BTSyncActivity.ready == true) {
			canvas.drawColor(Color.rgb(20, 20, 20)); // clear entire
														// screen black
		} else {
			//canvas.drawColor(Color.DKGRAY);
			canvas.drawText("PAUSED",868,440,mtext);
		}

		for (int horizontal = 0; horizontal < (WIDTH_Y / div)+1; horizontal++) // 200px/10=20
		{
			if (horizontal == ((WIDTH_Y / div) / 2)) {
				mpaint.setColor(Color.RED);
				canvas.drawLine(0, (horizontal * div), (WIDTH_X - 1),
						(horizontal * div), mpaint);
			} // mmid
			else {
				mpaint.setColor(Color.DKGRAY);
				canvas.drawLine(0, (horizontal * div), (WIDTH_X - 1),
						(horizontal * div), mpaint);
			} // mgrid
		}
		// draw vertical grids
		for (int vertical = 0; vertical < (WIDTH_X / div)+1; vertical++) // 260px/13=20
		{
			mpaint.setColor(Color.DKGRAY);
			canvas.drawLine((vertical * div), 0, (vertical * div),
					WIDTH_Y, mpaint); // mgrid
		}
		if ((BTSyncActivity.mode == 2)|| (BTSyncActivity.mode == 3))//continuous or triggered frame
		{
			if (you_can_draw == true) {
				for (int n = 0, x = 0; n < (SAMPLES_WIDTH-1) ; x = (x + (BTSyncActivity.rf*4)), n = (n + 1)) {
					canvas.drawLine(x, ((WIDTH_Y-1) - v_s[n]), (x + (BTSyncActivity.rf*4)),
							((WIDTH_Y-1) - v_s[n + 1]), mwave); // worked
				}
			}
		} else // roll mode
		{
			for (int n = 0; n < (SAMPLES_WIDTH-1); n++) {
				v_r[n] = v[n];
			}

		/*	for (int n = 0, x = 0; x < (WIDTH_X - BTSyncActivity.d); x = (x + BTSyncActivity.d), n = (n + 1)) {
				// mpaint.setColor(Color.GREEN);
				canvas.drawLine(x, ((WIDTH_Y - v_r[n])), (x + BTSyncActivity.d),
						((WIDTH_Y - v_r[n + 1])), mwave); // worked
			}*/
			for (int n = 0, x = 0; n < (SAMPLES_WIDTH-1) ; x = (x + (BTSyncActivity.rf*4)), n = (n + 1)) {
				canvas.drawLine(x, ((WIDTH_Y-1) - v_r[n]), (x + (BTSyncActivity.rf*4)),
						((WIDTH_Y-1) - v_r[n + 1]), mwave); // worked
			}			
		}
		switch (BTSyncActivity.range) {
		case 3:
			//sendMessage("1");
			canvas.drawText("2.5V/div",868,20,mtext);
			break;
		case 2:
			//sendMessage("2");
			canvas.drawText("0.25V/div",868,20,mtext);
			break;
		case 1:
			//sendMessage("3");
			canvas.drawText("0.025V/div",868,20,mtext);
			break;

		default:
			break;
		}
		if(BTSyncActivity.mode == 1){
			canvas.drawText(+(BTSyncActivity.rf*600)+"ms/div",0,20,mtext);
		}
		else{
			//rf*(6samples or 24px time)
			//canvas.drawText(+((BTSyncActivity.dt/256)*5)+"ms/div",0,20,mtext);
			switch (BTSyncActivity.ps) {
			case 8:
				canvas.drawText(+(BTSyncActivity.rf*0.1171875)+"ms/div",0,20,mtext);
				break;
			case 16:
				canvas.drawText(+(BTSyncActivity.rf*0.1875)+"ms/div",0,20,mtext);
				break;
			case 32:
				canvas.drawText(+(BTSyncActivity.rf*0.3515625)+"ms/div",0,20,mtext);
				break;
			case 64:
				canvas.drawText(+(BTSyncActivity.rf*0.65625)+"ms/div",0,20,mtext);
				break;
			case 128:
				canvas.drawText(+(BTSyncActivity.rf*1.2890625)+"ms/div",0,20,mtext);
				break;				
			default:
				break;
			}				
		}
		
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		gd.onTouchEvent(event);
		return true;
	}	
	
/*	public void clearscreen(Canvas canvas)
	{
		canvas.drawColor(Color.rgb(20, 20, 20));  //black
	}*/
//                android:textAppearance="?android:attr/textAppearanceMedium"
}
